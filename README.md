# OSES Web Site README
This is the source code to the OSES website.  It has deliberately been built using vanilla web technologies - the site will display nicely without the need for executing any Javascript.  There's no database attached, and no requirement for anything special in the development environment.  We did write the styles in [sass](https://github.com/sass/sass), so you should grab a free sass compiler, such as [node-sass](https://github.com/sass/node-sass) in order to modify the styles.

# Development workflow
Typically, you will want changes you make to show up while you work.  To enable that, we suggest the following
```bash
	node-sass --watch assets/styles/styles.scss -o assets/styles &
	python3 -m http.server &
```
And then navigate your browser to `http://localhost:8000/`

# Contributions
Contributions to this web site are welcomed!  Please submit a merge request, or simply tell us what things to fix.  Any contributions large enough to warrant a copyright must be released under the AGPL to be accepted.

